package com.seasuncc.servicedemo.utils;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

public class RequestData<T> {
    private int pageIndex = 1;
    private int pageSize = 10;
    private Page page;
    private T data;

    public Page getPage() {
        if (this.page == null) {
            this.page = new Page((long)this.pageIndex, (long)this.pageSize);
        } else {
            this.page.setSize((long)this.pageSize);
            this.page.setCurrent((long)this.pageIndex);
        }

        return this.page;
    }

    public RequestData() {
    }

    public int getPageIndex() {
        return this.pageIndex;
    }

    public int getPageSize() {
        return this.pageSize;
    }

    public T getData() {
        return this.data;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof RequestData)) {
            return false;
        } else {
            RequestData<?> other = (RequestData<?>) o;
            if (!other.canEqual(this)) {
                return false;
            } else if (this.getPageIndex() != other.getPageIndex()) {
                return false;
            } else if (this.getPageSize() != other.getPageSize()) {
                return false;
            } else {
                label40: {
                    Object this$page = this.getPage();
                    Object other$page = other.getPage();
                    if (this$page == null) {
                        if (other$page == null) {
                            break label40;
                        }
                    } else if (this$page.equals(other$page)) {
                        break label40;
                    }

                    return false;
                }

                Object this$data = this.getData();
                Object other$data = other.getData();
                if (this$data == null) {
                    if (other$data != null) {
                        return false;
                    }
                } else if (!this$data.equals(other$data)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof RequestData;
    }

    public String toString() {
        return "RequestData(pageIndex=" + this.getPageIndex() + ", pageSize=" + this.getPageSize() + ", page=" + this.getPage() + ", data=" + this.getData() + ")";
    }
}
