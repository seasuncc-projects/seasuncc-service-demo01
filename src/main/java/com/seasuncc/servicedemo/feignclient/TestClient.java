package com.seasuncc.servicedemo.feignclient;

import com.seasuncc.servicedemo.user.domain.User;
import com.seasuncc.servicedemo.utils.RequestData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * TODO: 类中文描述
 *
 * @author shidoudou
 * @date 2020/9/9
 */
@FeignClient(name = "seasuncc-service-demo",fallback = TestClientFallBack.class)
public interface TestClient {

    /**
     * TODO: 接口/方法中文描述
     * TODO: 伪代码
     *
     * @date:  2020/9/9

     * @return java.lang.String
     * @author  shidoudou
     * @modifer 请修改人员维护
     * @modifyTime  请修改人员维护
     */
    @RequestMapping("/user/list")
    List<User> userList(RequestData<User> requestData);
}
