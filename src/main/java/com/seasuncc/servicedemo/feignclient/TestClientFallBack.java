package com.seasuncc.servicedemo.feignclient;

import com.seasuncc.servicedemo.user.domain.User;
import com.seasuncc.servicedemo.utils.RequestData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * TODO: 类中文描述
 *
 * @author shidoudou
 * @date 2020/9/9
 */

@Component
public class TestClientFallBack implements TestClient{
    private static final Logger log = LoggerFactory.getLogger(TestClientFallBack.class);
    /**
     * TODO: 接口/方法中文描述
     * TODO: 伪代码
     *
     * @date:  2020/9/9

     * @return java.util.List<com.seasun.springfeign.vo.City>
     * @author  shidoudou
     * @modifer 请修改人员维护
     * @modifyTime  请修改人员维护
     */
    @Override
    public List<User> userList(RequestData<User> requestData) {
        log.info("调用失败");
        return null;
    }
}
