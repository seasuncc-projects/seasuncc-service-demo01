package com.seasuncc.servicedemo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * TODO: 类中文描述
 *
 * @author shidoudou
 * @date 2020/9/14
 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@MapperScan(
        basePackages = {"com.seasuncc.servicedemo.user.mapper"}
)
public class ServiceDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceDemoApplication.class, args);
    }

}
