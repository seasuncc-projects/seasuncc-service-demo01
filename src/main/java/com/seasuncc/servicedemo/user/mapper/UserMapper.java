package com.seasuncc.servicedemo.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.seasuncc.servicedemo.user.domain.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * TODO: 类中文描述
 *
 * @author shidoudou
 * @date 2020/9/9
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

    Page<User> query(Page page, @Param("data") User user);
}
