package com.seasuncc.servicedemo.user.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.seasuncc.servicedemo.user.domain.User;
import com.seasuncc.servicedemo.user.mapper.UserMapper;
import com.seasuncc.servicedemo.user.service.UserService;
import com.seasuncc.servicedemo.utils.RedisTemplateService;
import com.seasuncc.servicedemo.utils.RequestData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jws.soap.SOAPBinding;
import java.util.List;

/**
 * TODO: 类中文描述
 *
 * @author shidoudou
 * @date 2020/9/9
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private UserMapper mapper;

    @Autowired
    private RedisTemplateService redisTemplateService;

    @Override
    public List<User> getUserList(RequestData<User> requestData) {
        User user=requestData.getData();
        Page page=requestData.getPage();
        Page<User> page1=mapper.query(page,user);
        List<User> list=page1.getRecords();
        redisTemplateService.set("111",list);

        return list;
    }

    public static void main(String[] args) {
        System.out.println(11);
    }
}
