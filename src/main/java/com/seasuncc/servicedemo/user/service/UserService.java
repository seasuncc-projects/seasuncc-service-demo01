package com.seasuncc.servicedemo.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.seasuncc.servicedemo.user.domain.User;
import com.seasuncc.servicedemo.utils.RequestData;

import java.util.List;

/**
 * TODO: 类中文描述
 *
 * @author shidoudou
 * @date 2020/9/9
 */
public interface UserService extends IService<User> {

    List<User> getUserList(RequestData<User> requestData);
}
