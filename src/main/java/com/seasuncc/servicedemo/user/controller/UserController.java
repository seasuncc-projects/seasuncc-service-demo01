package com.seasuncc.servicedemo.user.controller;

import com.seasuncc.servicedemo.user.domain.User;
import com.seasuncc.servicedemo.user.service.UserService;
import com.seasuncc.servicedemo.utils.RequestData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * TODO: 类中文描述
 *
 * @author shidoudou
 * @date 2020/9/9
 */
@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/list")
    public List<User> userList(@RequestBody RequestData<User> requestData){
        return userService.getUserList(requestData);
    }

    public static void main(String[] args) {
        System.out.println(344);
    }
}
