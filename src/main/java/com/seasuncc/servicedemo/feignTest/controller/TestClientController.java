package com.seasuncc.servicedemo.feignTest.controller;

import com.seasuncc.servicedemo.feignTest.service.TestClientService;
import com.seasuncc.servicedemo.feignTest.vo.UserVo;
import com.seasuncc.servicedemo.user.domain.User;
import com.seasuncc.servicedemo.utils.RequestData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * TODO: 类中文描述
 *
 * @author shidoudou
 * @date 2020/9/9
 */
@RestController
@RequestMapping("/testclientA")
public class TestClientController {

    /**/
    @Autowired
    private TestClientService testClientService;

    @RequestMapping("/list")
    public List<User> getUserList(@RequestBody RequestData<User> requestData){
        UserVo userVo=new UserVo();
        return testClientService.getUserList(requestData);
    }

}
