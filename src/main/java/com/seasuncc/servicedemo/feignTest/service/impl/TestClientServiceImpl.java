package com.seasuncc.servicedemo.feignTest.service.impl;

import com.seasuncc.servicedemo.feignTest.service.TestClientService;
import com.seasuncc.servicedemo.feignclient.TestClient;
import com.seasuncc.servicedemo.user.domain.User;
import com.seasuncc.servicedemo.utils.RequestData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * TODO: 类中文描述
 *
 * @author shidoudou
 * @date 2020/9/9
 */
@Service
public class TestClientServiceImpl implements TestClientService {

    @Autowired
    private TestClient testClient;

    /**
     * TODO: 接口/方法中文描述
     * TODO: 伪代码
     *
     * @date:  2020/9/9

     * @return java.util.List<com.seasun.springfeign.vo.City>
     * @author  shidoudou
     * @modifer 请修改人员维护
     * @modifyTime  请修改人员维护
     */
    @Override
    public List<User> getUserList(RequestData<User> requestData) {
        List<User> list=testClient.userList(requestData);
        return list;
    }
}
