package com.seasuncc.servicedemo.feignTest.service;

import com.seasuncc.servicedemo.user.domain.User;
import com.seasuncc.servicedemo.utils.RequestData;

import java.util.List;

/**
 * TODO: 类中文描述
 *
 * @author shidoudou
 * @date 2020/9/9
 */
public interface TestClientService {
    /**
     * TODO: 接口/方法中文描述
     * TODO: 伪代码
     *
     * @date:  2020/9/9 
     
     * @return java.util.List<com.seasun.springfeign.vo.City> 
     * @author  shidoudou
     * @modifer 请修改人员维护
     * @modifyTime  请修改人员维护
     */
    List<User> getUserList(RequestData<User> requestData);
}
