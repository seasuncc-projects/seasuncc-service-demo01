package com.seasuncc.servicedemo.feignTest.vo;

import lombok.Data;

/**
 * TODO: 类中文描述
 *
 * @author shidoudou
 * @date 2020/9/9
 */
@Data
public class UserVo {

    private String id;

    /**
     * 用户名
     */
    private String userName;
    /**
     * 密码
     */
    private String passWord;
    /**
     * 逻辑删除(0-未删除,1-已删除)
     */
    private String delFlag;

    /**
     * 创建时间,允许为空,让数据库自动生成即可
     */
    private String createTime;
}
